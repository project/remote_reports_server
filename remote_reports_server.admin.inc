<?php

/**
 * @file
 *
 * Remote Reports Server module administration settings.
 */

/**
 * Get all API keys from database.
 *
 * @return array $query.
 */
function remote_reports_server_get_api_keys() {
  // Use Database API to retrieve API keys.
  $query = db_select('remote_reports_server_api_keys', 'api_key')
    ->fields('api_key')
    ->execute();

  return $query;
}

/**
 * Get API key from database with id.
 *
 * @param $id
 *  The id of the API key
 *
 * @return array $query.
 */
function remote_reports_server_get_api_key($api_key_id) {
  // Use Database API to retrieve the API key.
  $query = db_select('remote_reports_server_api_keys', 'api_key')
    ->fields('api_key')
    ->condition('id', $api_key_id, '=')
    ->execute()
    ->fetchAssoc();

  return $query;
}

/**
 * Inits remote_reports_server_config.
 */
function remote_reports_server_config() {
  $build = array();

  // Use our custom function to retrieve data.
  $keys = remote_reports_server_get_api_keys();
  // Array to contain rows for table render.
  $rows = array();

  // Table header
  $header = array(
    // Column 1
    'title' => array(
      'data' => t('Title'),
    ),
    // Column 2
    'api_key' => array(
      'data' => t('API key'),
    ),
    // Colum 3
    'actions' => array(
      'data' => t('Actions'),
    )
  );

  // Iterate over the result set and format as links.
  foreach ($keys as $key) {
    $rows[] = array(
      'data' => array(
        'title' => array(
          'data' => $key->title,
        ),
        'api_key' => array(
          'data' => $key->api_key,
        ),
        'actions' => array(
          'data' => l(t('Delete'), 'admin/config/system/remote_reports_server/' . $key->id . '/delete'),
        )
      ),
    );
  }

  // No content found
  if (empty($rows)) {
    $build['content']['#markup'] = '<p>' . t('No API keys found. Click on \'Add an API key\' to add one.') . '</p>';
  }
  else {
    // Pass data through theme function.
    $build['content'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No API keys found. Click on \'Add an API key\' to add one.'),
    );
  }

  return $build;
}

/**
 * Form to add an api key.
 */
function remote_reports_server_add_api_key_form($form, &$form_state, $api_key = '') {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title to remember what this key is for.'),
    '#required' => TRUE,
  );

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('The API key that will be allowed.'),
    '#default_value' => $api_key,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function remote_reports_server_add_api_key_form_submit($form, &$form_state) {
  // Insert the new API key.
  db_insert('remote_reports_server_api_keys')
    ->fields(array(
      'title' => $form_state['values']['title'],
      'api_key' => $form_state['values']['api_key'],
    ))
    ->execute();

  // Display a success message.
  drupal_set_message(t('The API key %api_key has been added.', array('%api_key' => $form_state['values']['title'])), 'status', TRUE);
  // Redirect to the default config page.
  $form_state['redirect'] = url('admin/config/system/remote_reports_server');
}

/**
 * Form to delete an API key
 */
function remote_reports_server_delete_api_key_confirm($form, &$form_state, $api_key_id) {
  $api_key = remote_reports_server_get_api_key($api_key_id);
  $form['api_key_id'] = array('#type' => 'value', '#value' => $api_key_id);
  $form['title'] = array('#type' => 'value', '#value' => $api_key['title']);
  return confirm_form($form,
    t('Confirm deletion'),
    'admin/config/system/remote_reports_server',
    t('Are you sure you want to delete the API key: %title?', array('%title' => $api_key['title'])),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implements hook_submit().
 */
function remote_reports_server_delete_api_key_confirm_submit($form, &$form_state) {
  if($form_state['values']['confirm']) {
    db_delete('remote_reports_server_api_keys')
      ->condition('id', $form_state['values']['api_key_id'])
      ->execute();

    // Display a success message.
    drupal_set_message(t('The API key %api_key has been deleted.', array('%api_key' => $form_state['values']['title'])), 'status', TRUE);
    // Redirect to the default config page.
    $form_state['redirect'] = url('admin/config/system/remote_reports_server');
  }
}

/**
 * Inits remote_reports_server_config.
 */
function remote_reports_server_unauthorized_requests() {
  $build = array();

  // Use our custom function to retrieve data.
  $requests = db_select('remote_reports_server_unauthorized_requests', 'r')
    ->fields('r')
    ->orderBy('date', 'DESC') // Newest first
    ->execute();
  // Array to contain rows for table render.
  $rows = array();

  // Table header
  $header = array(
    // Column 1
    'date' => array(
      'data' => t('Date'),
    ),
    // Column 2
    'ip_address' => array(
      'data' => t('IP address'),
    ),
    // Column 3
    'api_key' => array(
      'data' => t('API key'),
    ),
    // Colum 4
    'actions' => array(
      'data' => t('Actions'),
    )
  );

  // Iterate over the result set and format as links.
  foreach ($requests as $request) {
    $rows[] = array(
      'data' => array(
        'date' => array(
          'data' => $request->date,
        ),
        'ip_address' => array(
          'data' => $request->ip_address,
        ),
        'api_key' => array(
          'data' => $request->api_key,
        ),
        'actions' => array(
          'data' => l(t('Add API key'), 'admin/config/system/remote_reports_server/add/' . $request->api_key),
        )
      ),
    );
  }

  // No content found
  if (empty($rows)) {
    $build['content']['#markup'] = '<p>' . t('No unauthorized requests found.') . '</p>';
  }
  else {
    // Pass data through theme function.
    $build['content'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No unauthorized requests found.'),
    );
  }

  return $build;
}