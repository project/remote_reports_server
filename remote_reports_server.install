<?php
/**
 * @file
 *
 * Install/uninstall related functions.
 */

/**
 * Implements hook_schema().
 */
function remote_reports_server_schema() {
  $schema['remote_reports_server_api_keys'] = array(
    'description' => 'Authorized api keys to connect with Remote Reports Server.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for an api key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'title' => array(
        'description' => 'Title to remember what this api key is for.',
        'type' => 'text',
        'not null' => TRUE
      ),
      'api_key' => array(
        'description' => 'The api key that is authorized.',
        'type' => 'varchar',
        'length' => 1000,
        'not null' => TRUE
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['remote_reports_server_unauthorized_requests'] = array(
    'description' => 'Requests with api keys that are not authorized.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'ip_address' => array(
        'description' => 'The ip address from the requester',
        'type' => 'text',
        'not null' => TRUE
      ),
      'api_key' => array(
        'description' => 'The key that isn\'t authorized.',
        'type' => 'varchar',
        'length' => 1000,
        'not null' => TRUE
      ),
      'date' => array(
        'description' => 'The date of the request',
        'type' => 'timestamp',
        'mysql_type' => 'timestamp',
        'not null' => TRUE
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}
